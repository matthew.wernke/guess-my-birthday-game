import random
months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
]

username = input("Please enter your name: ")

month_integer = random.randint(0, 11)
rand_month = months[month_integer]
rand_year = random.randint(1900, 2022)
user_confirmation = input(f'{username}, is {rand_month} {rand_year} your birthday? Please enter yes/no: ')

while user_confirmation == 'no':
    print("Drat, Lemme try again!")
    month_integer = random.randint(0, 11)
    rand_month = months[month_integer]
    rand_year = random.randint(1900, 2022)
    user_confirmation = input(f'{username} is {rand_month} {rand_year} your birthday? Please enter yes/no: ')
    continue
if user_confirmation == 'yes':
    print("I knew it!")
    exit()
else:
    exit()
